
import java.util.Scanner;


public class Server{
	
	
	private String command;
	private boolean listening;
	private Thread listenerThread;
	private ServerConnectionListener serverListener;
	private Scanner scanner;
	
	 public Server()
	   {
		
		command = " ";
		listening = false;
		scanner = new Scanner(System.in);
		
		}
	     
	   
	 
	 public void startUp(){
		 	System.out.println("These are the following commands you can use:");
			System.out.println("Start listening");
			System.out.println("Shutdown");
			System.out.println("Stop listening");
			
			 
			 while(true){
				 

				 System.out.print("Press your command here:");
				 command = scanner.nextLine();
				 
				 if (command.equals("Stop listening") && listening == false)
					 System.out.println("The Server is already not listening right now");
				 
				 else if(command.equals("Start listening") && listening == true)
					 System.out.println("The Server is already listening");
				 
				 else  if (command.equals("Start listening")){
					 startListening();
					 pause(500);
				 }
				 
				 else if(command.equals("Stop listening")){
					 stopListening();
					 pause(500);
				 }
				 
				 else if(command.equals("Shutdown")){
					 shutdown();
				 	 }
				 
				 else  {
					pause(500);	 
					System.out.println("Invalid Command! The Commands are case Sensitive! Correct Commands are:");
					System.out.println("Start listening");
					System.out.println("Shutdown");
					System.out.println("Stop listening");
					System.out.println("");
				 }
				 	pause(250);
				
				 
	
			 }
	 
	 
	 
	 	}
	 
	 public void startListening(){
	
		serverListener = new ServerConnectionListener(69);
	
		 listenerThread = new Thread(serverListener, "listener");
		 listenerThread.start();
		 listening = true;
		 System.out.println("listening..");
	 }
	 
	 public void stopListening(){
		 serverListener.closeSocket();
		 listening = false;
		 System.out.println("Not listening..");
	 }
	 
	 public void shutdown(){
		 pause(250);
		 System.out.println("Shutting down the server..");
		 pause(150);
		 System.exit(0);
	 }
	 
	 public void pause(int milliseconds){
			try {
			    Thread.sleep(milliseconds);                 //1000 milliseconds is one second.
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
	 }
	 
	 
	
	 
	 public static void main(String[] args){
		 Server s = new Server();
		 s.startUp();
	 }
	 
	 
	 
	
	 
	

}