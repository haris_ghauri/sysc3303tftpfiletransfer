import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class ServerConnectionListener implements Runnable {
	
	private DatagramSocket receiveSocket;
	private DatagramPacket receivePacket;
	private Thread managerThread;
	private ServerConnectionManager serverConnectionManager;
	private int count;
	private int port;

	public ServerConnectionListener(int port){
		this.port = port;
		
		
		count = 0;
		 try {
	         receiveSocket = new DatagramSocket(port); 
	      } catch (SocketException se) {
	         se.printStackTrace();
	         System.exit(1);
	      }

	}

	@Override
	public void run() {
		
		System.out.println("port of the serverlistener is "+port);
		
		
		while(true){
		   try {        
		         System.out.println("Waiting..."); // so we know we're waiting
		         byte data[] = new byte[100];
				 receivePacket = new DatagramPacket(data, data.length);
		         receiveSocket.receive(receivePacket);
		      } catch (IOException e) {
		         System.out.print("Stopped Listening for some reason..");
		         //e.printStackTrace();
		    }
		   	
		   	System.out.println("Server received something with the following details" );
		   	Packet.displayReceivedPacket(receivePacket, "Listener");
		   
		   	 
		   	 if(Packet.isRequestValid(receivePacket)){
		     serverConnectionManager = new ServerConnectionManager(receivePacket);
			 managerThread = new Thread(serverConnectionManager, "connectionManager "+count );
			 managerThread.start();
			 count++;
			 System.out.println("request was valid");
		   	 }
		   	 
		   	 else{
		     System.out.println("Invalid Packet");
		   	 }
		   
		   
		   
		   
		}
	}
	
	public int getNumberOfClientsConnected(){
		return count;
	}
	
	public void closeSocket(){
		receiveSocket.close();
	}

}
