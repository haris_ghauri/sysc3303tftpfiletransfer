import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Client
{
	
	   private DatagramPacket sendPacket, receivePacket;
	   private DatagramSocket sendReceiveSocket;
	   
	   
	   public Client()
	   {
		   try {
		         sendReceiveSocket = new DatagramSocket();
		      } catch (SocketException se) {   
		         se.printStackTrace();
		         System.exit(1);
		      }
	   }
	   
	   
	   public void startUp() {
		   
		   try {
			sendPacket = Packet.makeWriteRequest("test.txt", "octet", InetAddress.getLocalHost(), 69);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		   
		   Packet.displaySendingPacket(sendPacket, "Client");   
		   
		   try {
	             sendReceiveSocket.send(sendPacket);
	          } catch (IOException e) {
	             e.printStackTrace();
	             System.exit(1);
	          }
	          System.out.println("Client: Packet sent.\n");
	          
	          
	          byte data[] = new byte[100];
	          receivePacket = new DatagramPacket(data, data.length);
	          System.out.println("Waiting to receive the packet");
	          try {
	             sendReceiveSocket.receive(receivePacket);
	          } catch(IOException e) {
	             e.printStackTrace();
	             System.exit(1);
	          }
	          
	          Packet.displayReceivedPacket(receivePacket, "Cleint");

		   
	   }
	   
	   
	   
	   
	   
	   
	   
	   public static void main(String args[])
	   {
		   
		   Client client = new Client();
		   client.startUp();
	   }
	
}