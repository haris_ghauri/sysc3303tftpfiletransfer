import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class ErrorSimulator{
	
	 private DatagramPacket sendPacket, receivedPacketClient, receivedPacketServer;
	   private DatagramSocket receiveSocket68, sendReceiveSocket, sendSocketClient;
	   
	   public ErrorSimulator()
	    {
	      try {

	         receiveSocket68 = new DatagramSocket(68);
	         sendReceiveSocket = new DatagramSocket();
	      } catch (SocketException se) {   // Can't create the socket.
	         se.printStackTrace();
	         System.exit(1);
	      }
	        
	    }
	   
	   public void sendAndReceive(){
	        
	        while(true){
	            
	            byte data[] = new byte[100];
	            receivedPacketClient = new DatagramPacket(data, data.length);
	            System.out.println("IntermediateHost: Waiting for Packet from the Client");
	            try{
	                receiveSocket68.receive(receivedPacketClient);
	            } catch (IOException e){
	            System.out.print("IO Exception: likely:");
	            System.out.println("Receive Socket Timed Out.\n" + e);
	            e.printStackTrace();
	            System.exit(1);
	            }
	           // displayReceivedPacket( receivedPacketClient);      
	            Packet.displayReceivedPacket(receivedPacketClient, "ErrorSimulator");
	      
	      
	     
	     
	     
	     
	     
	     
	     // Making the new sendPacket with the same content as receivedPacketClient
	            try{
	                sendPacket = new DatagramPacket(data, receivedPacketClient.getLength(), InetAddress.getLocalHost(), 69);
	            } catch (UnknownHostException e) {
	                e.printStackTrace();
	                System.exit(1);
	            }    
	            //displaySendingPacket( sendPacket);  
	            Packet.displaySendingPacket(sendPacket, "ErrorSimulator");
	            try {
	                sendReceiveSocket.send(sendPacket);
	            } catch (IOException e) {
	             e.printStackTrace();
	             System.exit(1);
	            }
	            System.out.println("Intermediate Host: Packet sent to Server.\n");  
	      
	      
	       
	       
	       
	       
	      
	            data = new byte[100];
	            receivedPacketServer  = new DatagramPacket(data, data.length);
	            System.out.println("IntermediateHost: Waiting for Packet from the Server");
	            try {
	                // Block until a datagram is received via sendReceiveSocket.
	                System.out.println("Waiting..."); 
	                sendReceiveSocket.receive(receivedPacketServer);
	            } catch(IOException e) {
	                e.printStackTrace();
	                System.exit(1);
	            }
	            //displayReceivedPacket( receivedPacketServer);
	            Packet.displayReceivedPacket(receivedPacketServer, "ErrorSimulator");
	      
	     
	     
	     
	     
	      
	            try {
	             // Construct a datagram socket and bind it to any available 
	             // port on the local host machine. This socket will be used to
	             // send and receive UDP Datagram packets.
	             sendSocketClient = new DatagramSocket();
	       
	            } catch (SocketException se) {   // Can't create the socket.
	                se.printStackTrace();
	                System.exit(1);
	            }
	          sendPacket = new DatagramPacket(data, receivedPacketServer.getLength(), receivedPacketClient.getAddress(), receivedPacketClient.getPort());
	          //displaySendingPacket( sendPacket);
	          Packet.displaySendingPacket(sendPacket, "ErrorSimulator");
	          try{
	              sendSocketClient.send(sendPacket);
	          }catch (IOException e){
	              e.printStackTrace();
	              System.exit(1);
	            }
	           System.out.println("Intermediate Host: packet sent to client.\n");
	            
	          
	            
	        
	        
	            sendSocketClient.close();
	            }
	        
	    }
	    

	    public static void main(String args[])
	   {
	      ErrorSimulator errorSimulator = new ErrorSimulator();
	      errorSimulator.sendAndReceive();
	   }
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	
}