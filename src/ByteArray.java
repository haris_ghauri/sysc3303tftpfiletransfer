import java.util.Arrays;

public class ByteArray {
	
	 public static byte[] makePacketByte(String fileName, String mode){
	       byte fileNameEncoded[] = fileName.getBytes();
	       byte modeEncoded[] = mode.getBytes();
	       byte ZeroByte[] = new byte[]{0}; 
	       byte[] combined = new byte[fileNameEncoded.length + ZeroByte.length + modeEncoded.length + ZeroByte.length];
	       
	       System.arraycopy(fileNameEncoded,0, combined, 0, fileNameEncoded.length);
	       System.arraycopy(ZeroByte, 0, combined, fileNameEncoded.length, ZeroByte.length);
	       System.arraycopy(modeEncoded, 0, combined,fileNameEncoded.length + ZeroByte.length, modeEncoded.length);
	       System.arraycopy(ZeroByte, 0, combined, fileNameEncoded.length + ZeroByte.length + modeEncoded.length, ZeroByte.length);
	       
	       return combined;
	       
	    }
	 
	 public static byte[] makeAcknowledgmentByte(int blockNumber){
			byte[] opcode = new byte[]{0,4};
			byte[] blkNumber = convertIntToByteArray(blockNumber);
			byte[] combined = new byte[opcode.length + blkNumber.length];
			System.arraycopy(opcode,0, combined, 0, opcode.length);
		    System.arraycopy(blkNumber, 0, combined, opcode.length, blkNumber.length);
		    return combined;	
		}
	 
	 public static byte[] makeDataByte(int blockNumber, byte[] data){
		 byte[] opcode = new byte[]{0,3};
		 byte[] blkNumber = convertIntToByteArray(blockNumber);
		 byte[] combined = new byte[opcode.length + blkNumber.length + data.length];
		 
		 
		   System.arraycopy(opcode,0, combined, 0, opcode.length);
	       System.arraycopy(blkNumber, 0, combined, opcode.length, blkNumber.length);
	       System.arraycopy(data, 0, combined,opcode.length + blkNumber.length, data.length);
	       return combined;
		 
	 }
	   
	   
	   public static byte[] addReadRequestToPacketByte(byte msg[]){ 
	       byte req[] = new byte[] {0, 1};
	       byte[] combined = new byte[ req.length + msg.length];
	       System.arraycopy(req,0, combined, 0, req.length);
	       System.arraycopy(msg, 0, combined, req.length, msg.length);
	       
	       return combined;
	    }
	    
	    
	    public static byte[] addWriteRequestToPacketByte(byte msg[]){
	       byte req[] = new byte[] {0, 2};
	       byte[] combined = new byte[req.length + msg.length];
	         
	         
	       System.arraycopy(req,0, combined, 0, req.length);
	       System.arraycopy(msg, 0, combined, req.length, msg.length);
	       
	       return combined;
	         
	    }
	    
	    
	    public static byte[] addInvalidRequestToPacketByte(byte msg[]){
	        byte req[] = new byte[] {1,2};
	        byte[] combined = new byte[msg.length + req.length];
	         
	         
	       System.arraycopy(req,0, combined, 0, req.length);
	       System.arraycopy(msg, 0, combined, req.length, msg.length);
	       
	       return combined;
	    }
	    
	   
	    public static byte[] convertIntToByteArray(int intNumber){
			intNumber = intNumber-32768;
		 	byte byteNumber[] = new byte[2];
			byteNumber[0] = (byte) (intNumber & 0xFF);
			byteNumber[1] = (byte) ((intNumber >> 8) & 0xFF);
			return byteNumber;
		}
		
		public static int convertByteArrayToInt(byte[] byteNumber){
			return (((int)byteNumber[1] << 8) | ((int)byteNumber[0] & 0xFF)) + 32768;
		}
		
		public static String getByteArrayAsString(byte[] Byte){
			return Arrays.toString(Byte);
		}

}
