import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class ServerConnectionManager implements Runnable {

	private DatagramSocket sendReceiveSocket;
	private DatagramPacket receivedPacket;
	private DatagramPacket sendPacket;

	
	public ServerConnectionManager(DatagramPacket receivedPacket){
		this.receivedPacket = receivedPacket;
		
		
		System.out.println("Connection manager has the received packet from Client");
		System.out.println("The client port is "+receivedPacket.getPort());
		 try {
	         sendReceiveSocket = new DatagramSocket();
	      } catch (SocketException se) {  
	         se.printStackTrace();
	         System.exit(1);
	      }
		 
		 
	}
	
	@Override
	public void run() {
		 
		
		  byte[] data = new byte[3];
		  Packet.displayReceivedPacket(receivedPacket, "Connection Manager");
		  
	      if(Packet.isReadRequest(receivedPacket)){
	      System.out.println("It was a readRequest\n");
	      sendPacket = Packet.makeDataBlock(0, data, receivedPacket.getAddress(), receivedPacket.getPort());
	      }
	      
	      else{
	      System.out.println("It was a writeRequest\n");
	      sendPacket = Packet.makeAcknowledgment(0, receivedPacket.getAddress(), receivedPacket.getPort());
	      }
	      
	
	      Packet.displaySendingPacket(sendPacket, "Connection Manager");
	      
	      
	      try {
	          sendReceiveSocket.send(sendPacket);
	       } catch (IOException e) {
	          e.printStackTrace();
	          System.exit(1);
	       }

	       System.out.println("Connection Manager: packet sent");
	       System.out.println("Transfer complete");
	      
	
	}

	
}
