import java.net.DatagramPacket;
import java.net.InetAddress;

public class Packet {
	

	public static  DatagramPacket makeReadRequest (String file, String mode, InetAddress address, int port ){
			byte[] msg =  ByteArray.addReadRequestToPacketByte(ByteArray.makePacketByte(file, mode));
			DatagramPacket packet = new DatagramPacket(msg, msg.length, address, port );
			return packet;
	}
	public static DatagramPacket makeWriteRequest (String file, String mode, InetAddress address, int port ){
		byte[] msg =  ByteArray.addWriteRequestToPacketByte(ByteArray.makePacketByte(file, mode));
		DatagramPacket packet = new DatagramPacket(msg, msg.length, address, port );
		return packet;
	}
	
	public static DatagramPacket makeAcknowledgment(int blockNumber, InetAddress address, int port){
		byte[] msg = ByteArray.makeAcknowledgmentByte(blockNumber);
		DatagramPacket packet = new DatagramPacket(msg,msg.length,address,port);
		return packet;
	}
	
	public static DatagramPacket makeDataBlock(int blockNumber, byte[] data, InetAddress address, int port){
		byte[] msg = ByteArray.makeDataByte(blockNumber, data);
		DatagramPacket packet = new DatagramPacket(msg,msg.length,address,port);
		return packet;
	}
	


	  public static boolean isRequestValid(DatagramPacket packet){   
		   int length = packet.getLength();
		   byte[] data = packet.getData();
	       int countFileNameCharacters = 0;
	       int countModeCharacters = 0;
	       if   (!( ((int)data[0] == 0  && (int)data[1] == 1) || (  (int)data[0] == 0  && (int)data[1] == 2) ))
	       return false;
	      
	       for (int i = 2 ; i < data.length ; i++){
	          if  ( (int)data[i] != 0)
	          countFileNameCharacters++;
	          
	          else
	          break;
	        }
	        
	        if  (!(countFileNameCharacters>0))
	        return false;
	        
	        for (int i = (2 + countFileNameCharacters + 1) ; i < data.length ; i++ ){
	          if ( (int)data[i] != 0)
	          countModeCharacters++;
	          
	          else
	          break;
	        }
	        
	        if (!(countModeCharacters > 0))
	           return false;

	        if ( (2 + countFileNameCharacters + 1 + countModeCharacters + 1) != length)
	            return false;
	    
	    return true;
	    }
	    
	    
	    public static boolean isReadRequest(DatagramPacket packet){
	    	byte[] data = packet.getData();
	    	
	        if ( (int)data[1] == 1)
	        return true;
	        
	        else
	        return false;
	    }
	    
	    
	    public static void displayReceivedPacket(DatagramPacket receivedPacket, String name){
	    	
	  	  	byte[] data = receivedPacket.getData();	
	        System.out.println(name+": Packet received:");
	        System.out.println("From host: " + receivedPacket.getAddress());
	        System.out.println("Host port: " + receivedPacket.getPort());
	        int len = receivedPacket.getLength();
	        System.out.println("Length: " + len);
	        System.out.println("Containing: ");
	        System.out.print("  In a Byte Array From: [");
	  
	        for (int z = 0 ; z < len ; z++){
	           int n = (int) data[z]; 
	           if (z == (len-1))
	           System.out.print(n);
	           
	           else
	           System.out.print(n+", ");
	          }
	          System.out.println("]");
	        // Form a String from the byte array.
	        String received = new String(data,0,len);   
	        System.out.println("  In String Form: "+received);
	        System.out.println("");
	        System.out.println("");
	  }
	  
	  
	  
	  public static void displaySendingPacket( DatagramPacket sendingPacket, String name){
	      
	  	  byte[] data = sendingPacket.getData();	
	        System.out.println(name+": Sending packet:");
	        System.out.println("To host: " + sendingPacket.getAddress());
	        System.out.println("Destination host port: " + sendingPacket.getPort());
	        int len = sendingPacket.getLength();
	        System.out.println("Length: " + len);
	        System.out.println("Containing: ");
	        System.out.print("  In a Byte Array From: [");
	        
	        for (int z = 0 ; z < len ; z++){
	           int n = (int) data[z]; 
	           if (z == (len-1))
	           System.out.print(n);
	           
	           else
	           System.out.print(n+", ");
	          }
	          System.out.println("]");
	        // Form a String from the byte array.
	        String sent = new String(data,0,len);   
	        System.out.println("  In String Form: "+sent);
	  }
	  
	
	

	    
	    public static void main(String[] args){
	    	
	    int inputNumber = 0;
	    byte[] check = ByteArray.convertIntToByteArray(inputNumber);
	    
	    System.out.println("My input number:"+inputNumber);
	    for (int i = 0; i<check.length ; i++){
	    	int num = check[i];
	    	System.out.println(num);
	    }
	    
	    byte data[] = new byte[]{1,2,3,5,7};
	    
	
	    System.out.println("My output number :"+ByteArray.convertByteArrayToInt(check));
	    
	    System.out.println("Byte array is: "+ByteArray.getByteArrayAsString(ByteArray.makeAcknowledgmentByte(inputNumber)));
	    
	    System.out.println("Data block in array form:" + ByteArray.getByteArrayAsString(ByteArray.makeDataByte(inputNumber,data)));
	    
	    }

}
